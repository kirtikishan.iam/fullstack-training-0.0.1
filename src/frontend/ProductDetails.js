import React, { useEffect, useState } from "react";

const ProductDetails = ({ id }) => {
  const [product, setProduct] = useState();
  const [view, setView] = useState();

  useEffect(async () => {
    const details = await fetch(`/products/${id}`, {
      method: 'GET',
    });
    const data = await details.json();
    console.log(data);
    setProduct(data.product);
  }, [id]);

  if (!product || product.id !== id) {
    console.log('loading');
    return <div>Loading ...</div>;
  }

  console.log('loaded', product.colour);
  return (
    <div>
      <h2>{product.brand} {product.model} {id}</h2>
      <ul>
        <li>Colours
          {product.colour?.map(colour => (
            <span key={colour.name}  style={{
              display: "inline-block",
              borderRadius: '50%',
              color: colour.hex
            }}>&nbsp;</span>
          ))}
        </li>
        <li>Capacity
          {product.memory?.map(capacity => (
            <span key={capacity.name}  style={{
              display: "inline-block",
            }}>{capacity.capacity} {capacity.price}</span>
          ))}
        </li>
      </ul>
    </div>
  );
}

export default ProductDetails;
