import React, { useEffect, useState } from "react";
import ProductDetails from  "./ProductDetails.js";

const ProductListing = () => {
  const [list, setList] = useState();
  const [view, setView] = useState();

  useEffect(async () => {
    const details = await fetch('/products', {
      method: 'GET',
    });
    const data = await details.json();
    setList(data.products);
  }, []);

  if (!list) {
    return null;
  }

  return (
    <div>
      <h1>Products</h1>
      <div>
        {list.map((product) => (
          <div key={product.id}>
            <h2>{product.brand} {product.model}</h2>
            <button onClick={() => setView(product.id)}>view</button>
          </div>
        ))}
      </div>
      {view && (
        <ProductDetails id={view} />
      )}
    </div>
  );
}

export default ProductListing;
