import fs from 'fs';
import path from 'path'
import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors';
import compression from 'compression'

const port = 3001

const app = express()
const corsOptions = {
  origin: ['http://127.0.0.1:3001', '*.oca-web.com'],
  credentials: true,
  optionsSuccessStatus: 200,
};
app.use(compression());
app.use(cors(corsOptions));
app.use(bodyParser.json())

app.get('', async (req, res) => {
  res.sendFile(path.resolve('./build/index.html'))
})
app.get('/', async (req, res) => {
  res.sendFile(path.resolve('./build/index.html'))
})
app.get('/static/js/:file', async (req, res) => {
  const { file } = req.params
  res.sendFile(path.resolve('./build/static/js/' + file))
})
app.get('/static/css/:file', async (req, res) => {
  const { file } = req.params
  res.sendFile(path.resolve('./build/static/css/' + file))
})

let details;
function getDetails() {
  if (details) {
    return details;
  }

  details = JSON.parse(fs.readFileSync(path.resolve('./mocks/productDetails.json')));
  return details;
}

app.get('/products', async (req, res) => {
  const details = getDetails();
  const list = details.map((product) => {
    return {
      id: product.id,
      brand: product.brand,
      model: product.model,
      stock: product.stock,
    };
  });

  res.json({
    products: list
  })
});

app.get('/products/:id', async (req, res) => {
  const details = getDetails();
  const detail = details.filter(product => product.id === parseInt(req.params.id, 10));

  res.json({
    product: detail[0]
  })
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
