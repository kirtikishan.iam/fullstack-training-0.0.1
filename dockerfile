FROM --platform=linux/amd64 node:16-alpine

WORKDIR /app
# Copy package.json and package-lock.json to the working directory
COPY package*.json ./
# Install the dependencies
RUN npm install
# Copy the application source code to the working directory
COPY . .
RUN npm run frontend:build

# Expose the port on which the application will run
EXPOSE 3000
# Set the command to run the application
CMD ["npm", "start"]